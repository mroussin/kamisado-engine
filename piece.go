package kamisado

const (
	White int = 1
	Black int = -1

	Empty int = 0

	Orange int = 1
	Blue   int = 2
	Purple int = 3
	Pink   int = 4
	Yellow int = 5
	Red    int = 6
	Green  int = 7
	Maroon int = 8

	WhiteOrange int = White * Orange
	WhiteBlue   int = White * Blue
	WhitePurple int = White * Purple
	WhitePink   int = White * Pink
	WhiteYellow int = White * Yellow
	WhiteRed    int = White * Red
	WhiteGreen  int = White * Green
	WhiteMaroon int = White * Maroon

	BlackOrange int = Black * Orange
	BlackBlue   int = Black * Blue
	BlackPurple int = Black * Purple
	BlackPink   int = Black * Pink
	BlackYellow int = Black * Yellow
	BlackRed    int = Black * Red
	BlackGreen  int = Black * Green
	BlackMaroon int = Black * Maroon
)

var (
	symbols = map[string]int{
		"-": Empty,
		"o": WhiteOrange, "b": WhiteBlue, "p": WhitePurple, "k": WhitePink, "y": WhiteYellow, "r": WhiteRed, "g": WhiteGreen, "m": WhiteMaroon,
		"O": BlackOrange, "B": BlackBlue, "P": BlackPurple, "K": BlackPink, "Y": BlackYellow, "R": BlackRed, "G": BlackGreen, "M": BlackMaroon,
	}
)

func pieceToString(piece int) string {
	for k, v := range symbols {
		if v == piece {
			return k
		}
	}
	return "-"
}

func stringPieceToInt(piece string) int {
	return symbols[piece]
}
