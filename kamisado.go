package kamisado

import "math"

type Kamisado struct {
	Board         *Board
	history       []Move
	currentPlayer int
}

func NewKamisado(fen string) *Kamisado {
	g := new(Kamisado)
	g.Board = parseFEN(fen)
	g.currentPlayer = Black
	g.history = []Move{}

	return g
}

func (k *Kamisado) PossibleMoves() []Move {
	var legalMoves = make([]Move, 25)
	currentPlayer := k.currentPlayer

	deltas := blackDelta
	if currentPlayer == White {
		deltas = whiteDelta
	}

	for _, v := range board64square {
		if k.Board.data[v]*currentPlayer > 0 {
			for _, delta := range deltas {
				for r := 1; r < size; r++ {
					if v+(r*delta)&0x88 > 0 {
						continue
					}
					if k.Board.data[v+(r*delta)] == Empty {
						legalMoves = append(legalMoves, Move{From: v, To: v + (r * delta)})
					} else {
						break
					}
				}

			}
		}
	}
	return legalMoves
}

func (k Kamisado) ASCIIArt() string {
	return k.Board.asciiArt()
}

func (k *Kamisado) IsCompleted() bool {
	if k.isBackrowWin() {
		return true
	}
	return false
}

func (k *Kamisado) isDeadlock() bool {
	if len(k.history) == 0 {
		return false
	}

	move := k.history[len(k.history)-1]
	currentPlayer := k.currentPlayer
	colorOfLastSquare := BoardColorsSquare[move.To]
	nextSquare := Empty

	seenSquares := make(map[int]bool)
	seenSquares[move.To] = true

	for {
		deltas := blackDelta
		if currentPlayer == White {
			deltas = whiteDelta
		}

		for _, v := range board64square {
			if k.Board.data[v] == colorOfLastSquare*currentPlayer {
				if seenSquares[v] {
					return true
				}
				nextSquare = v
				for _, delta := range deltas {
					if (v+delta)&0x88 > 0 {
						continue
					}
					if k.Board.data[v+delta] == Empty {
						return false
					}
				}
			}
		}
		seenSquares[nextSquare] = true
		colorOfLastSquare = BoardColorsSquare[nextSquare]
		currentPlayer *= -1
	}
}

func (k *Kamisado) isBackrowWin() bool {
	for _, v := range board64square[0:8] {
		if k.Board.data[v] < 0 {
			return true
		}
	}
	for _, v := range board64square[56:] {
		if k.Board.data[v] > 0 {
			return true
		}
	}
	return false
}

func (k *Kamisado) Move(m Move) bool {
	if k.isMoveValid(m) {
		k.currentPlayer *= -1
		k.Board.data[m.To] = k.Board.data[m.From]
		m.MovedPiece = k.Board.data[m.To]
		k.Board.data[m.From] = Empty
		k.history = append(k.history, m)
		return true
	}
	return false
}

func (k *Kamisado) currentPlayerCanMove() bool {
	if len(k.history) == 0 {
		return true
	}
	lastMove := k.history[len(k.history)-1]
	squareOfLastMove := lastMove.To
	colorOfLastSquare := BoardColorsSquare[squareOfLastMove]

	deltas := blackDelta
	if k.currentPlayer == White {
		deltas = whiteDelta
	}

	for _, v := range board64square {
		if k.Board.data[v] == colorOfLastSquare*k.currentPlayer {
			for _, delta := range deltas {
				if (v+delta)&0x88 > 0 {
					continue
				}
				if k.Board.data[v+delta] == Empty {
					return true
				}
			}
		}
	}

	return false
}

// Player can actually play a certain move, the game isn't deadlocked
func (k *Kamisado) isMoveValid(m Move) bool {
	//right color
	if len(k.history) != 0 {
		squareOfLastMove := k.history[len(k.history)-1].To
		colorOfLastSquare := BoardColorsSquare[squareOfLastMove]
		if math.Abs(float64(k.Board.data[m.From])) != float64(colorOfLastSquare) {
			return false
		}
	}

	//right direction
	diff := m.From - m.To
	if !(diff%0x11 == 0 || diff%0x10 == 0 || diff%0x0F == 0) || k.currentPlayer*diff <= 0 {
		return false
	}
	//don't pass through a piece
	startingPoint := m.To
	direction := 0

	if diff%0x11 == 0 {
		direction = 0x11
	} else if diff%0x10 == 0 {
		direction = 0x10
	} else {
		direction = 0x0F
	}

	nbOfSquares := (int(math.Abs(float64(diff))) / direction)
	for i := 0; i < nbOfSquares; i++ {
		if k.Board.data[startingPoint+(i*direction*k.currentPlayer)] != Empty {
			return false
		}
	}

	return true
}
