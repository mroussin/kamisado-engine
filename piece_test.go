package kamisado

import "testing"

func TestGivenWhiteOrangePieceNumber_ReturnCorresponingColor(t *testing.T) {
	expected := "o"
	result := pieceToString(WhiteOrange)

	if expected != result {
		t.Errorf("Expected %s piece but generated %s\n", expected, result)
	}
}

func TestGivenBlackPinkOrangePieceNumber_ReturnCorresponingColor(t *testing.T) {
	expected := "K"
	result := pieceToString(BlackPink)

	if expected != result {
		t.Errorf("Expected %s piece but generated %s\n", expected, result)
	}
}

func TestGivenWhiteMaroonPieceNumber_ReturnCorresponingColor(t *testing.T) {
	expected := "m"
	result := pieceToString(WhiteMaroon)

	if expected != result {
		t.Errorf("Expected %s piece but generated %s\n", expected, result)
	}
}
