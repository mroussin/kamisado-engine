package kamisado

import (
	"bytes"
	"sort"
	"strconv"
	"strings"
)

// Board represents a kamisado board
type Board struct {
	data       []int
	turn       int
	moveNumber int
}

func NewBoard() *Board {
	return &Board{data: make([]int, boardSize, boardSize)}
}

func (b *Board) generateFen() string {
	var fen = ""
	if len(b.data) == 0 {
		return ""
	}
	var empty = 0
	for i := 0; i < boardSize; i++ {
		v := b.data[i]

		if v == Empty {
			empty++
		} else {
			if empty > 0 {
				fen += strconv.Itoa(empty)
				empty = 0
			}

			fen += pieceToString(v)
		}
		if (i+1)&0x88 > 0 {
			if empty > 0 {
				fen += strconv.Itoa(empty)
			}

			if i != H1 {
				fen += "/"
			}

			empty = 0
			i += 8
		}
	}
	turn := "b"
	if b.turn == 1 {
		turn = "w"
	}

	return strings.Join([]string{fen, turn, strconv.Itoa(b.moveNumber)}, " ")
}

func (b *Board) asciiArt() string {
	values := []int{}
	for _, v := range SquareLookup {
		values = append(values, v)
	}
	sort.Ints(values)
	var buffer bytes.Buffer

	buffer.WriteString("\n   +------------------------+\n")
	for _, v := range values {
		if file(v) == 0 {
			buffer.WriteString(" ")
			buffer.WriteString(string("87654321"[rank(v)]))
			buffer.WriteString(" |")
		}
		/* empty piece */
		if b.data[v] == Empty {
			buffer.WriteString(" . ")
		} else {
			var symbol = pieceToString(b.data[v])
			buffer.WriteString(" ")
			buffer.WriteString(symbol)
			buffer.WriteString(" ")
		}

		if (v+1)&0x88 > 0 {
			buffer.WriteString("|\n")
		}
	}
	buffer.WriteString("   +------------------------+\n")
	buffer.WriteString("     a  b  c  d  e  f  g  h\n")

	return buffer.String()
}
