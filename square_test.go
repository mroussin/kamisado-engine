package kamisado

import "testing"

func TestGivenSquareOnFirstFile_ThenReturnFirstFile(t *testing.T) {
	expected := 0
	result := file(A1)

	if expected != result {
		t.Errorf("Expected %d piece but generated %d\n", expected, result)
	}
}

func TestGivenSquareOnSecondFile_ThenReturnSecondFile(t *testing.T) {
	expected := 1
	result := file(B4)

	if expected != result {
		t.Errorf("Expected %d piece but generated %d\n", expected, result)
	}
}

func TestGivenSquareOnFirstRank_ThenReturnFirstRank(t *testing.T) {
	expected := 2
	result := file(C1)

	if expected != result {
		t.Errorf("Expected %d piece but generated %d\n", expected, result)
	}
}

func TestGivenRankAndFile_ThenReturnTheSquare(t *testing.T) {
	expected := A8
	result := square(0, 0)

	if expected != result {
		t.Errorf("Expected %d piece but generated %d\n", expected, result)
	}
}
