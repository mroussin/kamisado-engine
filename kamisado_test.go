package kamisado

import "testing"

func TestGivenFreshGame_WithInvalidMove_ThenItsInvalid(t *testing.T) {
	k := NewKamisado(DefaultFen)
	invalidMove := Move{From: A8, To: G1}

	if moveIsInvalid := k.isMoveValid(invalidMove); moveIsInvalid {
		t.Errorf("Expected %+v to be invalid but was valid\n", invalidMove)
	}
}

func TestGivenFreshGame_WithDiagonalRightValidMove_ThenItsValid(t *testing.T) {
	k := NewKamisado(DefaultFen)
	validMove := Move{From: B8, To: G3}

	if moveIsValid := k.isMoveValid(validMove); !moveIsValid {
		t.Errorf("Expected %+v to be valid but was invalid\n", validMove)
	}
}

func TestGivenFreshGame_WithDiagonalLeftValidMove_ThenItsValid(t *testing.T) {
	k := NewKamisado(DefaultFen)
	validMove := Move{From: G8, To: B3}

	if moveIsValid := k.isMoveValid(validMove); !moveIsValid {
		t.Errorf("Expected %+v to be valid but was invalid\n", validMove)
	}
}

func TestGivenFreshGame_WithStraightValidMove_ThenItsValid(t *testing.T) {
	k := NewKamisado(DefaultFen)
	validMove := Move{From: C8, To: C2}

	if moveIsValid := k.isMoveValid(validMove); !moveIsValid {
		t.Errorf("Expected %+v to be valid but was invalid\n", validMove)
	}
}

func TestGivenFreshGame_ThenCurrentShouldBeAbleToPlay(t *testing.T) {
	k := NewKamisado(DefaultFen)

	if moveIsValid := k.currentPlayerCanMove(); !moveIsValid {
		t.Errorf("Expected black to be able to play but was unable\n")
	}
}

func TestGivenOneBlackMove_ThenWhiteShouldBeAbleToPlay(t *testing.T) {
	k := NewKamisado(DefaultFen)
	k.Move(Move{From: A1, To: G7, MovedPiece: BlackMaroon})

	if currentPlayerCanPlay := k.currentPlayerCanMove(); !currentPlayerCanPlay {
		t.Errorf("Expected white to be able to play but was unable\n")
	}
}

func TestGivenBlackOntoPinkSquare_ThenWhiteHasToPlayPinkColor(t *testing.T) {
	k := NewKamisado(DefaultFen)
	blackMove := Move{From: E1, To: F2, MovedPiece: BlackPink}
	k.Move(blackMove)
	whiteMove := Move{From: D8, To: D7, MovedPiece: WhitePink}

	if moveIsValid := k.isMoveValid(whiteMove); !moveIsValid {
		t.Errorf("Expected %+v to be valid but was invalid\n", whiteMove)
	}
}

func TestGivenBlackMove_ThenWhiteIsCurrentPlayer(t *testing.T) {
	k := NewKamisado(DefaultFen)
	blackMove := Move{From: C8, To: C2, MovedPiece: BlackPink}
	k.Move(blackMove)

	if k.currentPlayer != White {
		t.Errorf("Expected k.currentPlayer to be 1 but was %d\n", k.currentPlayer)
	}
}

func TestGivenBlackWinningMove_ThenGameIsCompleted(t *testing.T) {
	k := NewKamisado(DefaultFen)
	blackMove := Move{From: E8, To: H5, MovedPiece: BlackYellow}
	k.Move(blackMove)
	whiteMove := Move{From: D1, To: C2, MovedPiece: WhiteYellow}
	k.Move(whiteMove)
	winningBlackMove := Move{From: H5, To: D1, MovedPiece: BlackYellow}
	k.Move(winningBlackMove)

	if !k.IsCompleted() {
		t.Errorf("Expected game to be completed but was not\n")
	}
}

func TestGivenWhiteWinningMove_ThenGameIsCompleted(t *testing.T) {
	k := NewKamisado(DefaultFen)
	blackMove := Move{From: E8, To: E5, MovedPiece: BlackYellow}
	k.Move(blackMove)
	whiteMove := Move{From: A1, To: A4, MovedPiece: WhiteMaroon}
	k.Move(whiteMove)
	blackMove = Move{From: E5, To: D4, MovedPiece: BlackYellow}
	k.Move(blackMove)
	whiteWinningMove := Move{From: A4, To: E8, MovedPiece: WhiteMaroon}
	k.Move(whiteWinningMove)
	if !k.IsCompleted() {
		t.Errorf("Expected game to be completed but was not\n")
	}
}

func TestGivenNonDeadlockBoard_ThenItsNotDeadlocked(t *testing.T) {
	k := NewKamisado(DefaultFen)
	if k.isDeadlock() {
		t.Errorf("Expected game not to be deadlock but was\n")
	}
}

func TestGivenDeadlockBoard_ThenItsDeadlocked(t *testing.T) {
	k := NewKamisado("pkybgomr/8/8/8/8/8/8/MBOYGRKP b 2")
	k.Move(Move{From: B1, To: B7, MovedPiece: BlackBlue})
	k.Move(Move{From: F8, To: D6, MovedPiece: WhiteOrange})
	k.Move(Move{From: F1, To: F3, MovedPiece: BlackRed})
	k.Move(Move{From: D6, To: E2, MovedPiece: WhiteOrange})
	if k.isDeadlock() {
		t.Errorf("Expected game not to be deadlock but was\n")
	}
}
