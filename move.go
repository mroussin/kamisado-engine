package kamisado

import (
	"errors"
	"fmt"
	"regexp"
)

// Move on the board representation
type Move struct {
	From       int
	To         int
	MovedPiece int
	Content    int
}

type Moves []Move

func createMove(str string) (Move, error) {

	if m, _ := regexp.MatchString("^[a-h][1-8][a-h][1-8]$", str); !m {
		return Move{}, errors.New("invalid move")
	}

	from := str[:2]
	to := str[2:]

	return Move{From: SquareLookup[from], To: SquareLookup[to]}, nil
}

func printMoves(moves Moves) {
	str := fmt.Sprintf("%d available moves:\n", len(moves))
	for i, move := range moves {
		captured := ""
		if move.Content != Empty {
			captured = fmt.Sprintf(" {%s}", pieceToString(move.Content))
		}
		str += fmt.Sprintf("%s: %s..%s%s\t", pieceToString(move.MovedPiece), SquareMap[move.From], SquareMap[move.To], captured)

		if i%2 != 0 {
			str += "\n"
		}
	}
	fmt.Printf("%s\n", str)
}
